@isTest
private class GreeterTest {
  @isTest
  static void greet() {
    System.assertEquals(Greeter.greet('John'), 'Hello, my dear John from Greeter');
  }
}
