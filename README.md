# Hutte Recipe - CI/CD Incremental Deployment

> This recipe uses [sfdx-git-delta](https://github.com/scolladon/sfdx-git-delta) to incrementally deploy changes.

> **Note** PRs need to be merged via "Create a merge commit" or "Squash and merge" option to correctly detect the changes.

## Prerequisites

- a Gitlab repository with a valid sfdx project
- a target org authenticated with sfdx locally

## Steps

### Step 1

Create the following pipeline definition:

`.gitlab-ci.yml`

```yaml
trigger-validations:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      changes:
        - force-app/**/*
  trigger:
    include:
      - local: ".gitlab/pipelines/incremental-deploy.yml"
    strategy: depend
  variables:
    validateOnly: "true"
    baseRef: "$CI_MERGE_REQUEST_DIFF_BASE_SHA"
    targetRef: "HEAD"

trigger-deployment:
  stage: build
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - force-app/**/*
  trigger:
    include:
      - local: ".gitlab/pipelines/incremental-deploy.yml"
    strategy: depend
  variables:
    validateOnly: "false"
    baseRef: "HEAD^"
    targetRef: "HEAD"
```

Create the following Gitlab child pipeline for incremental deploy:

`.gitlab/pipelines/incremental-deploy.yml`

```yaml
image: salesforce/salesforcedx:latest-full

stages:
  - generate-delta-changes
  - deployment

generate-delta-changes:
  stage: generate-delta-changes
  script:
    - |
      echo y | sfdx plugins:install sfdx-git-delta
      if [ "$validateOnly" = "true" ]; then
        git merge "$baseRef" -m "Update with base ref"
      fi
      mkdir -p deltas
      sfdx sgd:source:delta --from "$baseRef" --to "$targetRef" --output deltas --generate-delta --ignore .forceignore
      echo "# deltas/package.xml"
      cat deltas/package/package.xml; echo ""
      echo "# deltas/destructiveChanges.xml"
      cat deltas/destructiveChanges/destructiveChanges.xml; echo ""
  rules:
    - when: on_success
  artifacts:
    paths:
      - deltas

deployment:
  stage: deployment
  rules:
    - when: on_success
  script:
    - |
      sfdx org login sfdx-url --set-default --sfdx-url-file <(echo "$SFDX_AUTH_URL_TARGET_ORG")
      deployFlags=(
        --manifest deltas/package/package.xml
        --postdestructivechanges deltas/destructiveChanges/destructiveChanges.xml
        --wait 30
        --testlevel RunLocalTests
        --verbose
      )
      if [ "$validateOnly" = "true" ]; then
        deployFlags+=( --checkonly )
      fi
      sfdx force:source:deploy "${deployFlags[@]}"
```

### Step 2

Setup Pipeline to `.gitlab-ci.yml`. (`Settings > CI/CD > General pipelines > CI/CD configuration file`).

### Step 3

Copy the value of `sfdxAuthUrl` to the clipboard.

```console
sfdx org display --verbose --json -o <MY_TARGET_ORG_ALIAS>
```

Create a Pipeline Variable (`Settings > CI/CD > Variables`):

| Name                       | Value                         |
| -------------------------- | ------------------------------ |
| `SFDX_AUTH_URL_TARGET_ORG` | <PASTE_THE_SFDX_AUTH_URL_HERE> |

Select `Mask Variable`. *Note that protected refers to exposing a variable to protected branches and tags and does not refer to mask or encrypt it, therefore this option is unchecked in order that the variable is accessible from any branch's pipeline execution.*

### Step 4

- Create a Merge Request with some changes in the `force-app` folder and verify the Action was run successfully
- Merge the Merge Request and verify the Action was run successfully

****
## Other Git Platforms

Find a similar recipe on other Git Platforms:

- [Github](https://github.com/hutte-recipes/cicd-incremental-deployment)
- [Azure DevOps](https://dev.azure.com/hutte-recipes/_git/cicd-incremental-deployment)
- [Bitbucket](https://bitbucket.org/hutte-recipes/cicd-incremental-deployment/src/main/)

****
